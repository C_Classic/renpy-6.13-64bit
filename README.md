# 64 bit patch for Ren'Py 6.13.12

## What is this?
This project is essentially just a 64 bit build of the Ren'Py visual novel engine version 6.13.12 compiled for modern Linux systems. It fixes a few issues with some old Ren'Py games, primarily:

* [Analogue: A Hate Story](http://ahatestory.com/) by Christine Love
* [Hate Plus](http://hateplus.com/) by Christine Love

## Why and when should I use this codebase?
If you can start and play the game, don't bother upgrading - even if you have a 64 bit system. There is no advantage of using this version.

However, if you are experiencing any of these problems, you might consider using this build:

* The game doesn't start at all (File not found error due to 64 bit only system)
* No graphics acceleration (soft rendering)

## Installation
The installation is simple. Just download this repository and move the contents to the `lib` folder of the game you want to patch. Make sure to overwrite the existing `python` file.

## Troubleshooting
### Audio problems
Open `lib/python` in your favorite text editor (*yes it has to be your favorite one - i.e. not vim :p*) and adapt line #8:

`export SDL_AUDIODRIVER=pulse`

You can also try removing it completely to fall back to the system default. 

## Bugs
I haven't tested every aspect of the above mentioned games. However, since this is a generic build there should not be many problems unless the game uses modified libraries. If you encounter some issues, feel free to open a bug report though.

## Development
If you consider building this project yourself, download the Ren'Py source code over on [renpy.org](http://www.renpy.org/release/6.13). When compiling, make sure all required development libraries (libpulse0-dev, libasound2-dev, ...) are installed. There are no changes to the actual source.